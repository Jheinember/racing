<?php 
	session_start();
	if($_SESSION['rol'] != 1)
	{
		header("location: ./");
	}
	
	include "../conexion.php";

	if(!empty($_POST))	
	{
		$alert='';
		if(empty($_POST['cedula']) || empty($_POST['nombre']) || empty($_POST['correo']) || empty($_POST['fechanacimiento']) ||empty($_POST['telefono']) ||empty($_POST['direccion']) ||empty($_POST['usuario']) ||empty($_POST['clave']) || empty($_POST['rol']))

		{
			$alert='<p class="msg_error">Todos los campos son obligatorios.</p>';
		}else{			

			$cedula = $_POST['cedula'];
			$nombre = $_POST['nombre'];
			$email  = $_POST['correo'];
			$fechanacimiento  = $_POST['fechanacimiento'];
			$telefono  = $_POST['telefono'];
			$direccion  = $_POST['direccion'];
			$user   = $_POST['usuario'];
			$clave  = md5($_POST['clave']);
			$rol    = $_POST['rol'];


			$query = mysqli_query($conection,"SELECT * FROM administrador WHERE usuario = '$user' OR correo = '$email' OR cedula = '$cedula' ");
			$result = mysqli_fetch_array($query);	
			
			if($result > 0){
				$alert='<p class="msg_error">El correo, la cedula o el usuario ya existe.</p>';
			}else{

				$query_insert = mysqli_query($conection,"INSERT INTO administrador(cedula,nombre,correo,fechanacimiento,telefono,direccion,usuario,clave,rol) VALUES('$cedula','$nombre','$email','$fechanacimiento','$telefono','$direccion','$user','$clave','$rol')");

				
				if($query_insert){
					$alert='<p class="msg_save"> Usuario creado correctamente.</p>';
				}else{
					$alert='<p class="msg_error"> Error al crear el usuario.</p>';
				}
			}
	    }	
	}
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<?php include "includes/scripts.php";?>	
	<title>Registro Usuario</title>
</head>
<body>	
	<?php include "includes/header.php"; ?>	
	<section id="container">
		<div class="form_register">
			<h1>Registro Usuario</h1>
			<hr>
			<div class="alert"><?php echo isset($alert) ? $alert : ''; ?></div>

			<form action="" method="post">

				
				<label for="nombre">Cedula</label>
				<input type="text" name="cedula" id="cedula" placeholder="Cedula: ">
					
				<label for="nombre">Nombre</label>
				<input type="text" name="nombre" id="nombre" placeholder="Nombre Completo: ">			

				<label for="correo">Correo Electronico</label>
				<input type="email" name="correo" id="correo" placeholder="Correo Electronico:">
				
				<label for="fechanacimiento">Fecha de Nacimiento</label>
				<input type="text" name="fechanacimiento" id="fechanacimiento" placeholder="YYYY-MM-DD">

				<label for="telefono">Telefono</label>
				<input type="text" name="telefono" id="telefono" placeholder="Telefono: ">	

				<label for="direccion">Direccion</label>
				<input type="text" name="direccion" id="direccion" placeholder="Direccion: ">	

				<label for="usuario">Usuario</label>
				<input type="text" name="usuario" id="usuario" placeholder="Usuario: ">

				<label for="clave">Clave </label>
				<input type="password" name="clave" id="clave" placeholder="Clave de acceso: ">

				<label for="rol">Tipo Usuario</label>

				<?php 
					$query_rol = mysqli_query($conection,"SELECT * FROM rol");
					mysqli_close($conection);
					$result_rol = mysqli_num_rows($query_rol);

				 ?>


				<select name= "rol" id="rol">
				<?php 
					if($result_rol > 0)
						{
							while($rol = mysqli_fetch_array($query_rol)){

				?>			
							<option value ="<?php echo $rol["idrol"]; ?>"><?php echo $rol["rol"] ?></option>
				
				<?php 

						} 
					} 

				 ?>	
				
				</select>
				<input type="submit" value="Crear usuario" class="btn_save">

			</form>
			



		</div>
	</section>


	<?php include "includes/footer.php"; ?>	
</body>
</html>