<nav>
			<ul>
				<li><a href="index.php">Inicio</a></li>
				<?php 
					if($_SESSION['rol'] == 1 ){

				 ?>
				<li class="principal">
					<a href="#">Usuarios</a>
					<ul>
						<li><a href="registro_usuario.php">Nuevo Usuario/Mecanico </a></li>
						<li><a href="lista_usuarios.php">Lista de Usuarios</a></li>
					</ul>
				</li>
			<?php } ?>
				</li><li class="principal">
					<a href="#">Clientes</a>
					<ul>
						<li><a href="registro_cliente.php">Nuevo Cliente</a></li>
						<li><a href="lista_clientes.php">Lista de Clientes</a></li>
					</ul>
				</li>

				<li class="principal">
					<a href="#">Vehiculos</a>
					<ul>
						<li><a href="lista_vehiculos.php">Lista Vehiculos</a></li>
					</ul>
				</li>

				<li class="principal">
					<a href="#">Servicios</a>
					<ul>
						<li><a href="registro_servicio.php">Nuevo Servicio</a></li>					
						<li><a href="
							lista_servicios
							.php">Lista de Servicios</a></li>
					</ul>
				</li>


				<li class="principal">
					<a href="#">REPUESTOS</a>
					<ul>
						<li><a href="registro_producto.php">Nuevo Producto</a></li>					
						<li><a href="lista.php">Lista de Productos</a></li>
					</ul>
				</li>
				<?php 
					if($_SESSION['rol'] == 1){

				 ?>
				<li class="principal">				
					<a href="#">Proveedores</a>
					<ul>
						<li><a href="registro_proveedor.php">Nuevo Proveedor</a></li>
						<li><a href="lista_proveedor.php">Lista de Proveedores</a></li>
					</ul>
				</li>
				<?php } ?>
				
				<li class="principal">
					<a href="#"><i class="far fa-file-alt"></i>Ventas</a>
					<ul>
						<li><a href="nueva_venta.php"><i class="fas fa-plus"></i>Nueva Venta</a></li>
						<li><a href="#"><i class="far fa-newspaper"></i> Ventas</a></li>
					</ul>
				</li>
			</ul>
		</nav>