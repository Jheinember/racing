<?php 
	session_start();
	include "../conexion.php"
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<?php include "includes/scripts.php" ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Nueva Venta</title>
</head>
<body>
	<?php include "includes/header.php" ?>

	<section id="container">
		<div class="title-page">
				<h1>Nueva Venta</h1>
			</div>
			<div class="datos_cliente">
				<div class="action_cliente">
				<h4>Datos del Cliente</h4>
				<a href="registro_cliente.php" class="btn_new btn_new_cliente">Nuevo Cliente</a>
			</div>
			<form name="form_new_cliente_venta" id="form_new_cliente_venta" class="datos">
				<input type="hidden" name="action" value="addCliente" >
				<input type="hidden" id="idcliente"	name="idcliente" value="" required>
				<div class="wd30">
					<label >Nit</label>
					<input type="text" name="nit_cliente" id="nit_cliente" >
				</div>
				<div class="wd30">
					<label >Nombre</label>
					<input type="text" name="nom_cliente" id="nom_cliente" disabled required>
				</div>
				<div class="wd30">
					<label >Telefono</label>
					<input type="number" name="tel_cliente" id="tel_cliente" disabled required>
				</div>
				<div class="wd100">
					<label >Direccion</label>
					<input type="text" name="dir_cliente" id="dir_cliente" disabled required>
				</div>
				<div id="div_registro_cliente" class="wd100">
					<button type="submit" class="btn_save">Guardar</button>
				</div>
			</form>	
		</div>

		<div class="datos_venta">
			<h4>Datos De Venta</h4>
			<div class="datos">
				<div class="wd50">
				<label>Vendedor</label>
				<p>Carlos Estrada Porras</p>
			</div>
				<div class="wd50">
					<label>Acciones</label>
					<div id="acciones_venta">
						<a href="#" class="btn_ok textcenter" id="btn_anular_venta">Anular</i>
						<a href="#" class="btn_new textcenter" id="btn_facturar_venta">Procesar</i>
					
			</section>
			<div>
				
				<table  class="tbl_venta">
					<thead>
					 <tr>
						<th width="200px">Codigo</th>
						<th>Descripcion</th>
						<th>Existencia</th>
						<th width="100px">Cantidad</th>
						<th class="textrigth">Precio</th>
						<th class="textrigth">Precio Total</th>
						<th> Accion </th>
					</tr>
					<tr>
						<td><input type="text" name="txt_cod_producto" id="txt_cod_producto"></td>
						<td id="txt_descripcion">-</td>
						<td id="txt_existencia">-</td>
						<td><input type="text" name="txt_cant_producto" id="txt_cant_producto" value="0"
						min="1" disabled></td> 
						<td id="txt_precio" class="textrigth">0.00</td>
						<td id="txt_precio_total" class="textrigth">0.00</td>
						<td> <a href="#" id="add_product_venta" class="link_add">Agregar</a></td>
					</tr>
					<tr>
						<th>Codigo</th>
						<th colspan="2">Descripcion</th>
						<th>Cantidad</th>
						<th class="textrigth">Precio</th>
						<th class="textrigth">Precio Total</th>
						<th> Accion</th>
					</tr>	
				</thead>
				<tbody id="detalle_venta">					
						<tr>    
						    <td>1</td>
							<td colspan="2">Valvulas</td>
							<td class="textcenter">1</td>
							<td class="textrigth">100.00</td>
							<td class="textrigth">100.00</td>
							<td class="">
								<a class="link_delete" href="#" onclick="event.preventDefault();
									del_product_detalle(1);">	
							</td>
						</tr>
						<tr>    
						    <td>20</td>
							<td colspan="2">Llantas</td>
							<td class="textcenter">5</td>
							<td class="textrigth">200.00</td>
							<td class="textrigth">2000.00</td>
							<td class="">
								<a class="link_delete" href="#" onclick="event.preventDefault();
									del_product_detalle(1);">	
							</td>
						</tr>			
				</tbody>
				<tfood>	
					<tr>
						<td colspan="5" class="textrigth">SUBTOTAL Q.</td>
						<td class="textrigth">1000.00 </td>	
					</tr>
					<tr>
						<td colspan="5" class="textrigth">IVA (12%)</td>
						<td class="textrigth">500.00 </td>	
					</tr>
					<tr>
						<td colspan="5" class="textrigth">TOTAL Q.</td>
						<td class="textrigth">1000.00 </td>	
					</tr>
			</tfood>			
		</table>			   
	</section>

	<?php include "includes/footer.php"?>

</body>
</html>