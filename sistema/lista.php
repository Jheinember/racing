<?php 
	session_start();
	include "../conexion.php"
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<?php include "includes/scripts.php";?>	
	<title>Lista De Productos</title>
</head>
<body>
	<?php include "includes/header.php"; ?>	
	<section id="container">

		<h1>Lista De Productos </h1>
		<a href="registro_producto.php" class="btn_new">Crear Producto</a>
		
		
		<form action="buscar_cliente.php" method="get" class="
		form_search">
			<input type="text" name="busqueda" id="busqueda" placeholder="Buscar">
			<input type="submit" value="Buscar" class="btn_search">
			</form>
		<table>
			<tr>
				<th>Codigo Producto</th>
				<th>Descripcion</th>
				<th>Precio</th>
				<th>Proveedor</th>				
				<th>Existencia</th>
				<th>Foto</th>			
				<th>Acciones</th>
			</tr>
		<?php 	
		    //Paginador
		    $sql_registe = mysqli_query($conection,"SELECT COUNT(*) as total_registro FROM cliente WHERE estatus = 1 ");
		    $result_register = mysqli_fetch_array($sql_registe);
		    $total_registro = $result_register['total_registro'];

		    $por_pagina = 7;

		    if(empty($_GET['pagina']))
		    {
		    	$pagina = 1;
		    }else{
		    	$pagina = $_GET['pagina'];
		    }

		    $desde = ($pagina-1) * $por_pagina;
		    $total_paginas = ceil($total_registro / $por_pagina);

		    $query = mysqli_query($conection,"SELECT* FROM producto 
		    								  WHERE estatus = 1 
		    								  ORDER BY refproducto DESC LIMIT $desde,$por_pagina");
		    mysqli_close($conection);

					$result = mysqli_num_rows($query);
					if($result > 0){	
						while($data = mysqli_fetch_array($query)) {
							if($data['foto'] != 'img-	+_producto.png'){
								$foto = 'img/uploads/'.$data['foto'];
							}else{
								$foto = 'img/'.$data['foto'];
							}

					?>	
						<tr>
							<td><?php echo $data ["refproducto"]; ?></td>
							<td><?php echo $data ["descripcion"]; ?></td>
							<td><?php echo $data ["precio"]; ?></td>
							<td><?php echo $data ["proveedor"]; ?></td>
							<td><?php echo $data ["existencia"]; ?></td>
							<td class="img_producto"><img src="<?php echo $foto; ?>" ></td>

							<?php if($_SESSION['rol'] == 1 ){ ?>
							<td>
								<a  class="link_add add_product" product="<?php echo $data ["refproducto"]; 
										?>" href="#"><i class="fa fa-plus"></i>Agregar</a>								
								|	
								<a  class="link_edit"  href="editar_producto.php?id=<?php echo $data ["refproducto"]; ?>">Editar</a>								
								|								
									<a  class="link_delete" href="eliminar_confirmar_producto.php?id=<?php echo $data ["refproducto"]; ?>">Eliminar</a>
								<?php } ?>	
								
							</td>
						</tr>

				<?php
						}
					}
				?>					

				</table>
				<div class="paginador">
					<ul>
						<?php 
							if($pagina !=1)
							{
							
						 ?>
						<li><a href="?pagina=<?php echo 1; ?>">|<</a></li>
						<li><a href="?pagina=<?php echo $pagina-1; ?>"><<</a></li>
					<?php 
						 }
						    for ($i=1; $i <= $total_paginas; $i++) 
						    { 
								# code...
								if($i == $pagina)
								{
									echo '<li class="pageSelected">'.$i.'</li>';
								}else{
									echo '<li><a href="?pagina='.$i.'">'.$i.'</a></li>';
								}								
							}
							if($pagina != $total_paginas)
							{
					?>							
						<li><a href="?pagina=<?php echo $pagina+ 1; ?>">>></a></li>
						<li><a href="?pagina=<?php echo $total_paginas; ?> ">>|</a></li>
					<?php } ?>
					</ul>					

				</div>
	</section>
	<?php include "includes/footer.php"; ?>	
</body>
</html>