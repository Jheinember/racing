<?php 
	session_start();	
	if($_SESSION['rol'] !=1 )	
	{
		header ("location: ./");
	}
	include "../conexion.php"
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<?php include "includes/scripts.php";?>	
	<title>Lista De Servicios</title>
</head>
<body>
	<?php include "includes/header.php"; ?>	
	<section id="container">

		<h1>Lista De Servicios </h1>
		<a href="registro_servicio.php.php" class="btn_new">Crear Servicio</a>
		
		
		<form action="buscar_servicio.php" method="get" class="
		form_search">
			<input type="text" name="busqueda" id="busqueda" placeholder="Buscar">
			<input type="submit" value="Buscar" class="btn_search">
			</form>
		<table>
			<tr>
				<th>Referencia Servicio</th>
				<th>Nombre Servicio</th>
				<th>Descripcion</th>			
				<th>Precio</th>
				<th>Fecha Registro</th>				
				<th>Acciones</th>
				
			</tr>
		<?php 	
		    //Paginador
		    $sql_registe = mysqli_query($conection,"SELECT COUNT(*) as total_registro FROM servicio  ");
		    $result_register = mysqli_fetch_array($sql_registe);
		    $total_registro = $result_register['total_registro'];

		    $por_pagina = 7;

		    if(empty($_GET['pagina']))
		    {
		    	$pagina = 1;
		    }else{
		    	$pagina = $_GET['pagina'];
		    }

		    $desde = ($pagina-1) * $por_pagina;
		    $total_paginas = ceil($total_registro / $por_pagina);

		    $query = mysqli_query($conection,"SELECT* FROM servicio  ORDER BY refservicio ASC LIMIT $desde,$por_pagina");
		    mysqli_close($conection);

					$result = mysqli_num_rows($query);
					if($result > 0){	

						while($data = mysqli_fetch_array($query)) {

							$formato = 'Y-m-d H:i:s';
							$fecha = DateTime::createFromFormat($formato,$data["date_add"]);


					?>	
						<tr>
							<td><?php echo $data ["refservicio"]; ?></td>
							<td><?php echo $data ["nombreservicio"]; ?></td>
							<td><?php echo $data ["descripcion"]; ?></td>
							<td><?php echo $data ["precio"]; ?></td>
							<td><?php echo $fecha->format('d-m-Y'); ?></td>

							<td>
								<a  class="link_edit"  href="editar_servicio.php?id=<?php echo $data ["codproveedor"]; ?>">Editar</a>
									|								
									<a  class="link_delete" href="eliminar_confirmar_servicio.php?id=<?php echo $data ["codproveedor"]; ?>">Eliminar</a>				
							</td>
						</tr>

				<?php
						}
					}
				?>					

				</table>
				<div class="paginador">
					<ul>
						<?php 
							if($pagina !=1)
							{
							
						 ?>
						<li><a href="?pagina=<?php echo 1; ?>">|<</a></li>
						<li><a href="?pagina=<?php echo $pagina-1; ?>"><<</a></li>
					<?php 
						 }
						    for ($i=1; $i <= $total_paginas; $i++) 
						    { 
								# code...
								if($i == $pagina)
								{
									echo '<li class="pageSelected">'.$i.'</li>';
								}else{
									echo '<li><a href="?pagina='.$i.'">'.$i.'</a></li>';
								}								
							}
							if($pagina != $total_paginas)
							{
					?>							
						<li><a href="?pagina=<?php echo $pagina+ 1; ?>">>></a></li>
						<li><a href="?pagina=<?php echo $total_paginas; ?> ">>|</a></li>
					<?php } ?>
					</ul>					

				</div>
	</section>
	<?php include "includes/footer.php"; ?>	
</body>
</html>