<?php
	session_start();
		
	include "../conexion.php";

	if(!empty($_POST))	
	{
		$alert='';
		if(empty($_POST['idvehiculo']) || empty($_POST['color']) || empty($_POST['marca']) || empty($_POST['ano']) ||empty($_POST['telefono']) ||empty($_POST['modelo']) )
		{
			$alert='<p class="msg_error">Todos los campos son obligatorios.</p>';
		}else{			

			$identificador = $_POST['id'];
			$idvehiculo = $_POST['idvehiculo'];
			$cedula = $_POST['cedula'];
			$color = $_POST['color'];
			$marca  = $_POST['marca'];
			$ano  = $_POST['ano'];
			$modelo  = $_POST['modelo'];

			

				$sql_update = mysqli_query($conection,"UPDATE vehiculo 
														   SET idvehiculo=$idvehiculo, 
														   cedula='$cedula',color='$color', 
														   marca='$marca',ano ='$ano',
														   modelo='$modelo'
														   WHERE idValue = $identificador ");

								
				if($sql_update){
					$alert='<p class="msg_save">Vehiculo actualizado correctamente.</p>';
				}else{
					$alert='<p class="msg_error">Error al actualizar el Vehiculo.</p>';
				
				}
			}
	    }		    
	

	//Mostrar Datos
	if(empty($_REQUEST['id']))
	{
		header('Location: lista_vehiculos.php');
		mysqli_close($conection);
	}	

	$identificador = $_REQUEST['id'];

	$sql = mysqli_query($conection,"SELECT * FROM vehiculo WHERE idValue = $identificador ");
	$result_sql = mysqli_num_rows($sql);

	if($result_sql == 0){
		header('Location: lista_vehiculos.php'); 
	}else{
		
		while($data = mysqli_fetch_array($sql)) {

			$identificador= $data['idValue'];
			$idvehiculo = $_POST['idvehiculo'];
			$cedula = $_POST['cedula'];
			$color = $_POST['color'];
			$marca  = $_POST['marca'];
			$ano  = $_POST['ano'];
			$modelo  = $_POST['modelo'];
		}	
	}

 ?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<?php include "includes/scripts.php";?>	
	<title>Actualizar Vehiculo</title>
</head>
<body>	
	<?php include "includes/header.php"; ?>	
	<section id="container">
		<div class="form_register">
			<h1>Actualizar Vehiculo</h1>
			<hr>
			<div class="alert"><?php echo isset($alert) ? $alert : ''; ?></div>

			<form action="" method="post">

				<input type="hidden" name="id" value="<?php echo $id; ?>">

				
				<label for="idvehiculo">Placa</label>
				<input type="text" name="idvehiculo" id="idvehiculo" placeholder="Placa del vehiculo" value="<?php echo $idvehiculo; ?>">
					
				<label for="color">Color</label>
				<input type="text" name="color" id="color" placeholder="Color" value="<?php echo $color; ?>">			

				<label for="marca">Marca</label>
				<input type="text" name="marca" id="marca" placeholder="Marca" value="<?php echo $marca; ?>">
				
				<label for="ano">Año</label>
				<input type="number" name="ano" id="ano" placeholder="Año" value="<?php echo $ano; ?>">

				<label for="modelo">Modelo</label>
				<input type="text" name="modelo" id="modelo" placeholder="Modelo" value="<?php echo $modelo; ?>">	


				
				<input type="submit" value="Actualizar Vehiculo" class="btn_save">

			</form>			



		</div>
	</section>
	<?php include "includes/footer.php"; ?>	
</body>
</html>