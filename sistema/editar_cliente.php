<?php
	session_start();
		
	include "../conexion.php";

	if(!empty($_POST))	
	{
		$alert='';
		if(empty($_POST['cedula']) || empty($_POST['nombre']) || empty($_POST['correo']) || empty($_POST['fechanacimiento']) ||empty($_POST['telefono']) ||empty($_POST['direccion']) )
		{
			$alert='<p class="msg_error">Todos los campos son obligatorios.</p>';
		}else{			

			$idCliente = $_POST['id'];
			$cedula = $_POST['cedula'];
			$nombre = $_POST['nombre'];
			$email  = $_POST['correo'];
			$fechanacimiento  = $_POST['fechanacimiento'];
			$telefono  = $_POST['telefono'];
			$direccion  = $_POST['direccion'];

			$result = 0;

			if(is_numeric($cedula) and $cedula !=0)
			{			 
				$query = mysqli_query($conection,"SELECT * FROM cliente 
												  WHERE(cedula = '$cedula' AND idcliente != $idCliente) 
													");
				$result = mysqli_fetch_array($query);
				$result = count($result);	
			}

			if($result > 0){
				$alert='<p class="msg_error">La cedula ya existe, ingrese otra.</p>';
			}else{

				if($cedula == '')
				{
					$init = 0;
				}

				$sql_update = mysqli_query($conection,"UPDATE cliente 
														   SET cedula=$cedula, nombre='$nombre', correo='$email', fechanacimiento='$fechanacimiento',
														   	   telefono = '$telefono', direccion='$direccion'
														   WHERE idcliente= $idCliente and estatus = 1");

								
				if($sql_update){
					$alert='<p class="msg_save">Cliente actualizado correctamente.</p>';
				}else{
					$alert='<p class="msg_error">Error al actualizar el cliente.</p>';
				
				}
			}
	    }		    
	}

	//Mostrar Datos
	if(empty($_REQUEST['id']))
	{
		header('Location: lista_clientes.php');
		mysqli_close($conection);
	}	

	$idcliente = $_REQUEST['id'];

	$sql = mysqli_query($conection,"SELECT * FROM cliente WHERE idcliente= $idcliente "); 
	mysqli_close($conection);
	$result_sql = mysqli_num_rows($sql);

	if($result_sql == 0){
		header('Location: lista_clientes.php'); 
	}else{
		
		while($data = mysqli_fetch_array($sql)) {

			$idcliente= $data['idcliente'];
			$cedula= $data['cedula'];
			$nombre = $data['nombre'];
			$correo = $data['correo'];
			$fechanacimiento = $data['fechanacimiento'];
			$telefono = $data['telefono'];
			$direccion = $data['direccion'];
		}	
	}			
 ?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<?php include "includes/scripts.php";?>	
	<title>Actualizar Cliente</title>
</head>
<body>	
	<?php include "includes/header.php"; ?>	
	<section id="container">
		<div class="form_register">
			<h1>Actualizar Cliente</h1>
			<hr>
			<div class="alert"><?php echo isset($alert) ? $alert : ''; ?></div>

			<form action="" method="post">

				<input type="hidden" name="id" value="<?php echo $idcliente; ?>">

				
				<label for="cedula">Cedula</label>
				<input type="number" name="cedula" id="cedula" placeholder="Cedula" value="<?php echo $cedula; ?>">
					
				<label for="nombre">Nombre</label>
				<input type="text" name="nombre" id="nombre" placeholder="Nombre Completo" value="<?php echo $nombre; ?>">			

				<label for="correo">Correo Electronico</label>
				<input type="email" name="correo" id="correo" placeholder="Correo Electronico" value="<?php echo $correo; ?>">
				
				<label for="fechanacimiento">Fecha de Nacimiento</label>
				<input type="text" name="fechanacimiento" id="fechanacimiento" placeholder="YYYY-MM-DD" value="<?php echo $fechanacimiento; ?>">

				<label for="telefono">Telefono</label>
				<input type="number" name="telefono" id="telefono" placeholder="Telefono" value="<?php echo $telefono; ?>">	

				<label for="direccion">Direccion</label>
				<input type="text" name="direccion" id="direccion" placeholder="Direccion Completa" value="<?php echo $direccion; ?>">

				
				<input type="submit" value="Actualizar Cliente" class="btn_save">

			</form>			



		</div>
	</section>
	<?php include "includes/footer.php"; ?>	
</body>
</html>