<?php 
	session_start();
	include "../conexion.php";

	if(!empty($_POST))	
	{
		$alert='';
		if(empty($_POST['refservicio']) || empty($_POST['nombreservicio']) || empty($_POST['descripcion']) ||empty($_POST['precio']))
		{
			$alert='<p class="msg_error">Todos los campos son obligatorios.</p>';
		}else{			

			$refservicio = $_POST['refservicio'];
			$nombreservicio = $_POST['nombreservicio'];
			$descripcion  = $_POST['descripcion'];
			$precio  = $_POST['precio'];
			$usuario_id  = $_SESSION['idUser'];


			$query_insert = mysqli_query($conection,"INSERT INTO servicio(refservicio,nombreservicio,descripcion,precio,usuario_id) 
				VALUES('$refservicio','$nombreservicio','$descripcion','$precio','$usuario_id')");


			
				if($query_insert){
					$alert='<p class="msg_save"> Servicio guardado correctamente.</p>';
				}else{
					$alert='<p class="msg_error"> Error al guardar el servicio.</p>';
				}					
	    }
	    mysqli_close($conection);		
	}
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<?php include "includes/scripts.php";?>	
	<title>Registro Servicio</title>
</head>
<body>	
	<?php include "includes/header.php"; ?>	
	<section id="container">
		<div class="form_register">
			<h1>Registro Servicio</h1>
			<hr>
			<div class="alert"><?php echo isset($alert) ? $alert : ''; ?></div>

			<form action="" method="post">

				
				<label for="refservicio">Referencia Del Servicio </label>
				<input type="text" name="refservicio" id="refservicio" placeholder="Referencia Del Servicio ">
					
				<label for="nombreservicio">Nombre del Servicio</label>
				<input type="text" name="nombreservicio" id="nombreservicio" placeholder="Nombre del Servicio: ">			

				<label for="descripcion">Descripcion del Servicio </label>
				<input type="text" name="descripcion" id="descripcion" placeholder="Descripcion: ">

				<label for="precio">Precio del Servicio</label>
				<input type="number" name="precio" id="precio" placeholder="Precio: ">

				
				<input type="submit" value="Guardar Servicio" class="btn_save">

			</form>
			



		</div>
	</section>


	<?php include "includes/footer.php"; ?>	
</body>
</html>