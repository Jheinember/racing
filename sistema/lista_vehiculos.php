<?php 
	session_start();
	include "../conexion.php"
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<?php include "includes/scripts.php";?>	
	<title>Lista De Vehiculos</title>
</head>
<body>
	<?php include "includes/header.php"; ?>	
	<section id="container">

		<h1>Lista De Vehiculos </h1>
		<a href="registro_vehiculo.php" ></a>
		
		
		<form action="buscar_vehiculo.php" method="get" class="
		form_search">
			<input type="text" name="busqueda" id="busqueda" placeholder="Buscar">
			<input type="submit" value="Buscar" class="btn_search">
			</form>
		<table>
			<tr>
				<th>ID</th>
				<th>Placa Del Vehiculo</th>
				<th>Propietario Del Vehiculo</th>
				<th>Color</th>
				<th>Marca</th>
				<th>Año</th>
				<th>Modelo</th>	
				<th>Fecha De Ingreso</th>					
				<th>Acciones</th>
			</tr>
		<?php 	
		    //Paginador
		    $sql_registe = mysqli_query($conection,"SELECT COUNT(*) as total_registro FROM vehiculo ");
		    $result_register = mysqli_fetch_array($sql_registe);
		    $total_registro = $result_register['total_registro'];

		    $por_pagina = 7;

		    if(empty($_GET['pagina']))
		    {
		    	$pagina = 1;
		    }else{
		    	$pagina = $_GET['pagina'];
		    }

		    $desde = ($pagina-1) * $por_pagina;
		    $total_paginas = ceil($total_registro / $por_pagina);

		    $query = mysqli_query($conection,"SELECT* FROM vehiculo
		    								  ORDER BY idValue ASC LIMIT $desde,$por_pagina");
		    mysqli_close($conection);

					$result = mysqli_num_rows($query);
					if($result > 0){	
						while($data = mysqli_fetch_array($query)) {

					?>	
						<tr>
							<td><?php echo $data ["idValue"]; ?></td>
							<td><?php echo $data ["idvehiculo"]; ?></td>
							<td><?php echo $data ["cedula"]; ?></td>
							<td><?php echo $data ["color"]; ?></td>
							<td><?php echo $data ["marca"]; ?></td>
							<td><?php echo $data ["ano"];?></td>
							<td><?php echo $data ["modelo"]; ?></td>
							<td><?php echo $data ["fechaingreso"]; ?></td>

							<td>
								<a  class="link_edit"  href="editar_vehiculo.php?id=<?php echo $data ["idValue"]; ?>">Editar</a>
								
								|								
									<a  class="link_delete" href="eliminar_confirmar_vehiculo.php?id=<?php echo $data ["idValue"]; ?>">Eliminar</a>
								
								
							</td>
						</tr>

				<?php
						}
					}
				?>					

				</table>
				<div class="paginador">
					<ul>
						<?php 
							if($pagina !=1)
							{
							
						 ?>
						<li><a href="?pagina=<?php echo 1; ?>">|<</a></li>
						<li><a href="?pagina=<?php echo $pagina-1; ?>"><<</a></li>
					<?php 
						 }
						    for ($i=1; $i <= $total_paginas; $i++) 
						    { 
								# code...
								if($i == $pagina)
								{
									echo '<li class="pageSelected">'.$i.'</li>';
								}else{
									echo '<li><a href="?pagina='.$i.'">'.$i.'</a></li>';
								}								
							}
							if($pagina != $total_paginas)
							{
					?>							
						<li><a href="?pagina=<?php echo $pagina+ 1; ?>">>></a></li>
						<li><a href="?pagina=<?php echo $total_paginas; ?> ">>|</a></li>
					<?php } ?>
					</ul>					

				</div>
	</section>
	<?php include "includes/footer.php"; ?>	
</body>
</html>