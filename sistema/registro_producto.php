<?php 
	session_start();
	if($_SESSION['rol'] != 1)
	{
		header("location: ./");
	}
	
	
	include "../conexion.php";

	if(!empty($_POST))	
	{			
		$alert='';
		if(empty($_POST['proveedor'])|| empty($_POST['refproducto']) || empty($_POST['descripcion']) || empty($_POST['precio']) || $_POST['precio'] <=0 || $_POST['existencia'] <= 0 ||empty($_POST['existencia']))
		{
			$alert='<p class="msg_error">Todos los campos son obligatorios.</p>';
		}else{			

			$refproducto = $_POST['refproducto'];
			$descripcion = $_POST['descripcion'];
			$proveedor  = $_POST['proveedor'];
			$precio  = $_POST['precio'];
			$existencia  = $_POST['existencia'];
			$administrador_id  = $_SESSION['idUser'];

			$foto = $_FILES['foto'];
			$nombre_foto  = $foto['name'];
			$type         = $foto['type'];
			$url_temp     = $foto['tmp_name'];

			$imgProducto = 'img_producto.png';
			
			if($nombre_foto != '')
			{
				$destino = 'img/uploads/';
				$img_nombre = 'img_'.md5(date('d-m-Y H:m:s'));
				$imgProducto = $img_nombre.'.jpg';
				$src = $destino.$imgProducto;

			}
					$query_insert = mysqli_query($conection,"INSERT INTO producto(refproducto,descripcion,proveedor,precio,existencia,administrador_id,foto) 
						VALUES('$refproducto','$descripcion','$proveedor','$precio','$existencia','$administrador_id','$imgProducto')");
			
				if($query_insert){
					if($nombre_foto != ''){
						move_uploaded_file($url_temp,$src);
					}					
					$alert='<p class="msg_save"> Producto guardado correctamente.</p>';
				}else{
					$alert='<p class="msg_error"> Error al guardar el Producto.</p>';
				}
			}					
	    }		
	  
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<?php include "includes/scripts.php";?>	
	<title>Registro Productoo</title>
</head>
<body>	
	<?php include "includes/header.php"; ?>	
	<section id="container">
		<div class="form_register">
			<h1>Registro Productoo</h1>
			<hr>
			<div class="alert"><?php echo isset($alert) ? $alert : ''; ?></div>

			<form action="" method="post" enctype="multipart/form-data">
				<label for="proveedor">Proveedor</label>
				<?php 
					$query_proveedor = mysqli_query($conection,"SELECT codproveedor, proveedor FROM proveedor WHERE estatus = 1 ORDER BY proveedor ASC");

					$result_proveedor = mysqli_num_rows($query_proveedor);
					mysqli_close($conection);
				 ?>
				 
				<select name="proveedor" id="proveedor">
				<?php 
					if($result_proveedor > 0){							
						while($proveedor = mysqli_fetch_array($query_proveedor)){
				?>	
					<option value ="<?php echo $proveedor['codproveedor']; ?>"><?php echo 
								$proveedor['proveedor']; ?>
					</option>	
				<?php 
							}
						}
				?>

				</select>


				<label for="refproducto">Referencia Producto</label>
				<input type="number" name="refproducto" id="refproducto" placeholder="Referencia">						
				<label for="descripcion">Nombre Producto</label>
				<input type="text" name="descripcion" id="descripcion" placeholder="Nombre Producto: ">						
				
				<label for="precio">Precio</label>
				<input type="number" name="precio" id="precio" placeholder="Precio producto: ">	

				<label for="existencia">Existencia</label>
				<input type="number" name="existencia" id="existencia" placeholder="Existencia: ">

				<div class="photo">
					<label for="foto">Foto</label>
				        <div class="prevPhoto">
				        <span class="delPhoto notBlock">X</span>
				        <label for="foto"></label>
				        </div>
				        <div class="upimg">
				        <input type="file" name="foto" id="foto">
				        </div>
				        <div id="form_alert"></div>
				</div> 

				
				<input type="submit" value="Guardar Producto" class="btn_save">

			</form>
			

		</div>
	</section>


	<?php include "includes/footer.php"; ?>	
</body>
</html>