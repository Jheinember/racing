<?php 
	session_start();
	
	include "../conexion.php";

	if(!empty($_POST))	
	{	
		
		$alert='';
		if(empty($_POST['cedula']) || empty($_POST['nombre']) || empty($_POST['correo']) || empty($_POST['fechanacimiento']) ||empty($_POST['telefono']) ||empty($_POST['direccion']))
		{
			$alert='<p class="msg_error">Todos los campos son obligatorios.</p>';
		}else{			

			$cedula = $_POST['cedula'];
			$nombre = $_POST['nombre'];
			$email  = $_POST['correo'];
			$fechanacimiento  = $_POST['fechanacimiento'];
			$telefono  = $_POST['telefono'];
			$direccion  = $_POST['direccion'];
			$administrador_id  = $_SESSION['idUser'];

			$idvehiculo = $_POST['idvehiculo'];
			$color = $_POST['color'];
			$marca  = $_POST['marca'];
			$ano = $_POST['ano'];
			$modelo = $_POST['modelo'];

			$result = 0;	
			
			if(is_numeric($cedula) and $cedula !=0 )
			{
				$query = mysqli_query($conection,"SELECT * FROM cliente WHERE cedula = '$cedula' ");
				$result = mysqli_fetch_array($query);					
			}

			if($result > 0){
				$alert='<p class="msg_error">El numero de Cedula ya existe.</p>';
			}else{

				$query_insert = mysqli_query($conection,"INSERT INTO cliente(cedula,nombre,correo,fechanacimiento,telefono,direccion,administrador_id) 
														 VALUES('$cedula','$nombre','$email','$fechanacimiento','$telefono','$direccion','$administrador_id')");

				$query_insert = mysqli_query($conection,"INSERT INTO vehiculo (cedula,idvehiculo,color,marca,ano,modelo) 
					VALUES('$cedula','$idvehiculo','$color','$marca','$ano','$modelo')");


				if($query_insert){
					$alert='<p class="msg_save"> Cliente guardado correctamente.</p>';
				}else{
					$alert='<p class="msg_error"> Error al guardar el cliente.</p>';
				}
			}					
	    }
	    mysqli_close($conection);		
	}
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<?php include "includes/scripts.php";?>	
	<title>Registro Cliente</title>
</head>
<body>	
	<?php include "includes/header.php"; ?>	
	<section id="container">
		<div class="form_register">
			<h1>Registro Cliente</h1>
			<hr>
			<div class="alert"><?php echo isset($alert) ? $alert : ''; ?></div>

			<form action="" method="post">

				
				<label for="cedula">Cedula</label>
				<input type="number" name="cedula" id="cedula" placeholder="Cedula: ">
					
				<label for="nombre">Nombre</label>
				<input type="text" name="nombre" id="nombre" placeholder="Nombre Completo: ">			

				<label for="correo">Correo Electronico</label>
				<input type="email" name="correo" id="correo" placeholder="Correo Electronico:">
				
				<label for="fechanacimiento">Fecha de Nacimiento</label>
				<input type="text" name="fechanacimiento" id="fechanacimiento" placeholder="YYYY-MM-DD">

				<label for="telefono">Telefono</label>
				<input type="number" name="telefono" id="telefono" placeholder="Telefono: ">	

				<label for="direccion">Direccion</label>
				<input type="text" name="direccion" id="direccion" placeholder="Direccion Completa: ">



				<label for="nombrevehiculo"> DATOS DEL VEHICULO </label>
					
				<label for="idvehiculo">Placa </label>
				<input type="text" name="idvehiculo" id="idvehiculo" placeholder="Placa De Vehiculo ">		

				<label for="color">Color </label>
				<input type="text" name="color" id="color" placeholder="Color del Vehiculo">
				
				<label for="marca">Marca</label>
				<input type="text" name="marca" id="marca" placeholder="Marca del Vehiculo">

				<label for="ano">Año </label>
				<input type="number" name="ano" id="ano" placeholder="Año Del Vehiculo: ">	

				<label for="modelo">Modelo Del Vehiculo</label>
				<input type="text" name="modelo" id="modelo" placeholder="Modelo del Vehiculo">
				
				<input type="submit" value="Guardar Datos Del Cliente" class="btn_save">

			</form>
			



		</div>
	</section>


	<?php include "includes/footer.php"; ?>	
</body>
</html>