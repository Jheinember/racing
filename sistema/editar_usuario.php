<?php
	session_start();
	if($_SESSION['rol'] != 1)
	{
		header("location: ./");
	} 
	
	include "../conexion.php";

	if(!empty($_POST))	
	{
		$alert='';
		if(empty($_POST['cedula']) || empty($_POST['nombre']) || empty($_POST['correo']) || empty($_POST['fechanacimiento']) ||empty($_POST['telefono']) ||empty($_POST['direccion']) ||empty($_POST['usuario']) || empty($_POST['rol']))
		{
			$alert='<p class="msg_error">Todos los campos son obligatorios.</p>';
		}else{			

			$idUsuario = $_POST['id'];
			$cedula = $_POST['cedula'];
			$nombre = $_POST['nombre'];
			$email  = $_POST['correo'];
			$fechanacimiento  = $_POST['fechanacimiento'];
			$telefono  = $_POST['telefono'];
			$direccion  = $_POST['direccion'];
			$user   = $_POST['usuario'];
			$clave  = md5($_POST['clave']);
			$rol    = $_POST['rol'];

			 
			$query = mysqli_query($conection,"SELECT * FROM administrador 
													   WHERE(usuario = '$user' AND idusuario != $idUsuario) 
													   OR (correo = '$email' AND idusuario != $idUsuario) 
													   OR (cedula = '$cedula' AND idusuario != $idUsuario) ");
			
			
			$result = mysqli_fetch_array($query);			 
			$result = count ($result);

			if($result > 0){
				$alert='<p class="msg_error">El correo, la cedula o el usuario ya existe.</p>';
			}else{

				if(empty($_POST['clave']))
				{
					$sql_update = mysqli_query($conection,"UPDATE administrador 
														   SET cedula='$cedula',nombre='$nombre',correo='$email',fechanacimiento='$fechanacimiento',
														   telefono='$telefono',direccion='$direccion',usuario='$user',rol= '$rol' 
														   WHERE idusuario= $idUsuario ");

				}
				
				if($sql_update){
					$alert='<p class="msg_save">Usuario actualizado correctamente.</p>';
				}else{
					$alert='<p class="msg_error">Error al actualizar el usuario.</p>';
				}
			}
	    }		    
	}

	//Mostrar Datos
	if(empty($_REQUEST['id']))
	{
		header('Location: lista_usuarios.php');
		mysqli_close($conection);
	}	

	$iduser = $_REQUEST['id'];

	$sql = mysqli_query($conection,"SELECT u.idusuario,u.cedula,u.nombre,u.correo,u.usuario,u.fechanacimiento,u.telefono,u.direccion,(u.rol) as idrol ,(r.rol) as rol FROM administrador u INNER JOIN rol r on u.rol = r.idrol WHERE idusuario= $iduser and estatus = 1"); 

	mysqli_close($conection);
	$result_sql = mysqli_num_rows($sql);

	if($result_sql == 0){
		header('Location: lista_usuarios.php'); 
	}else{
		$option =''; 
		while($data = mysqli_fetch_array($sql)) {

			$iduser= $data['idusuario'];
			$cedula= $data['cedula'];
			$nombre = $data['nombre'];
			$correo = $data['correo'];
			$fechanacimiento = $data['fechanacimiento'];
			$telefono = $data['telefono'];
			$direccion = $data['direccion'];
			$usuario = $data['usuario'];
			$idrol = $data['idrol'];
			$rol = $data['rol'];

			if($idrol == 1){
				$option  = '<option value= "'.$idrol. '" select>'.$rol.'</option>';

			}else if($idrol == 2){
				$option  = '<option value= "'.$idrol. '" select>'.$rol.'</option>';
			}	
		}	
	}			
 ?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<?php include "includes/scripts.php";?>	
	<title>Actualizar Usuario</title>
</head>
<body>	
	<?php include "includes/header.php"; ?>	
	<section id="container">
		<div class="form_register">
			<h1>Actualizar Usuario</h1>
			<hr>
			<div class="alert"><?php echo isset($alert) ? $alert : ''; ?></div>

			<form action="" method="post">

				<input type="hidden" name="id" value="<?php echo $iduser; ?>">
				<label for="nombre">Cedula</label>
				<input type="text" name="cedula" id="cedula" placeholder="Cedula"value="<?php echo $cedula;?>">

				<label for="nombre">Nombre</label>
				<input type="text" name="nombre" id="nombre" placeholder="Nombre Completo"value="<?php echo $nombre;?>">

				<label for="correo">Correo Electronico</label>
				<input type="email" name="correo" id="correo" placeholder="Correo Electronico"value="<?php echo $correo;?>">

				<label for="fechanacimiento">Fecha de Nacimiento</label>
				<input type="text" name="fechanacimiento" id="fechanacimiento" placeholder="YYYY-MM-DD"value="<?php echo $fechanacimiento;?>">

				<label for="telefono">Telefono</label>
				<input type="text" name="telefono" id="telefono" placeholder="Telefono"value="<?php echo $telefono;?>">

				<label for="direccion">Direccion</label>
				<input type="text" name="direccion" id="direccion" placeholder="Direccion"value="<?php echo $direccion;?>">	
				<label for="usuario">Usuario</label>
				<input type="text" name="usuario" id="usuario" placeholder="Usuario"value="<?php echo $usuario;?>">

				<label for="clave">Clave </label>
				<input type="password" name="clave" id="clave" placeholder="Clave de acceso: ">

				<label for="rol">Tipo De Usuario </label>

				<?php 
					include "../conexion.php";
					$query_rol = mysqli_query($conection,"SELECT * FROM rol");
					mysqli_close($conection);
					$result_rol = mysqli_num_rows($query_rol);

				 ?>


				<select name= "rol" id="rol" class="notItemOne">
				<?php 
					echo $option;
					if($result_rol > 0)
						{
							while($rol = mysqli_fetch_array($query_rol)){
				?>			
							<option value="<?php echo $rol["idrol"]; ?>"><?php echo $rol["rol"] ?></option>
				
				<?php 

						} 
					} 

				 ?>	
				
				</select>
				<input type="submit" value="Actualizar usuario" class="btn_save">

			</form>
			



		</div>
	</section>
	<?php include "includes/footer.php"; ?>	
</body>
</html>